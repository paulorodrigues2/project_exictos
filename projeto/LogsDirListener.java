package projeto;

import java.io.File;
import java.nio.file.Files;
import java.util.List;
import java.util.TimerTask;

public class LogsDirListener extends TimerTask{
	String logsFilesPath;
	
	LogsDirListener() {
		
		logsFilesPath = "C:\\Logs";
		
		
		
	}
	
public void run() {
		
	
	 
	  
	File[] filesOnDir = getFiles(); // listar ficheiros da pasta C:\logs 

		for(File file: filesOnDir) {
			String fileName = file.getName();
			if(Application.instance().readFiles().contains(fileName))  // Create simpletask
				continue;

			MyFileReader reader = new MyFileReader(fileName);
			reader.start();
			System.out.println("Reading a file \n" + fileName);
			Application.instance().addReadFile(fileName);
		}
	}
private File[] getFiles()
{
	File folder = new File(logsFilesPath);
	File[] listOfFiles = folder.listFiles();
	
	return listOfFiles;
}
}
