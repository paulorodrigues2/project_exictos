package projeto;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;




public class machine {


	
	private String Macdress;
	private String state;
	private String datetimer;
	private String idMachine;

	

	public String getDatetimes() {
		String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH/mm/ss").format(Calendar.getInstance().getTime());
		System.out.println(timeStamp );
		return timeStamp;
	}


	private void setDatetime(String datetime) {
		this.datetimer = datetime;
	}


	public String  getState() {
		return state;
	}
	
	


	public String setState(String states) {
		return state = states;
	}


	public void setMacdress(String macdress) {
		Macdress = macdress;
	}
	

	public String getMacdress() {
	      InetAddress ip;
	    try {

	        ip = InetAddress.getLocalHost();
	       // System.out.println("Current IP address : " + ip.getHostAddress());

	        NetworkInterface network = NetworkInterface.getByInetAddress(ip);

	        byte[] mac = network.getHardwareAddress();

	        System.out.print("Current MAC address : ");

	        StringBuilder sb = new StringBuilder();
	        for (int i = 0; i < mac.length; i++) {
	            sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));        
	        }
	        
	        Macdress = sb.toString();
	        System.out.println(Macdress);
	        
	      
	                

	    } catch (UnknownHostException e) {

	        e.printStackTrace();

	    } catch (SocketException e){

	        e.printStackTrace();

	    }
		return Macdress;
		

	}


	public String getIdMachine() {
		return idMachine;
	}


	public void setIdMachine(String idMachine) {
		this.idMachine = idMachine;
	}
}
	

