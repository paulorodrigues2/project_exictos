package projeto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import loggers.ErrorNodejs;
import loggers.ErrorWebsphere;

import org.apache.commons.io.FileUtils;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.Transaction;

import com.google.gson.Gson;

class MyFileReader extends Thread {

	String filePath;
	String fileName;
	Database db;
	String destinationFolder; // destionationDB;

	List<ErrorWebsphere> ErrorsWebsphereList;
	List<ErrorNodejs> ErrorsNodejslist;

	MyFileReader(String aFileName) {

		fileName = aFileName;
		filePath = "C:\\Logs\\";
		destinationFolder = "C:\\Trash";

		// destionationDB = "C:\\Database\\graph_db";
		ErrorsWebsphereList = new ArrayList<ErrorWebsphere>();
		ErrorsNodejslist = new ArrayList<ErrorNodejs>();
		db = new Database();

	}

	@Override
	public void run() {
		File fileNamePath = FileUtils.getFile(fileName);
		File DestinationPathFolder = FileUtils.getFile(destinationFolder);

		try {

			if (fileName.endsWith(".txt")) {
				createValues();
			}

			if (fileName.startsWith("Service")) {

				System.out.println("READ.. READ .. READ ...");
				readTxt();
			} else if (fileName.startsWith("Hardware")) {

				readJson();

			}

			FileUtils.moveFileToDirectory(fileNamePath, DestinationPathFolder,
					true);
			/*
			 * System.out.println("Moving file \b" + fileName.toString() +
			 * " to another Path \b" + DestinationPathFolder.toString());
			 */
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (ErrorsWebsphereList.size() > 0) {

			// db.connect();
			// System.out.println("Database are connecting");
			/**/
			for (int i = 0; i < ErrorsWebsphereList.size(); i++) {

				System.out.println(ErrorsWebsphereList.get(i)
						.getTime_occurred().toString());
				System.out.println(ErrorsWebsphereList.get(i).getWhere()
						.toString());
				System.out.println(ErrorsWebsphereList.get(i).getMessage()
						.toString());
			}

			db.saveWebsphereErrorstoDB(ErrorsWebsphereList);
		}
		if (ErrorsNodejslist.size() > 0) {

			db.saveNodejsToDB(ErrorsNodejslist);
		}

	}

	private void readTxt() throws IOException {

		File fileNamePath = FileUtils.getFile(filePath + fileName);

		// Get values from Group 0 to 5;
		Map<Integer, List<String>> errorsFromLogFile = new HashMap<Integer, List<String>>();
		// Get the values from comments
		ArrayList<String> stackTrace = null;

		boolean ControlCheck = false;

		// String date = (Date)formatter.parse(date);

		String pattern = "(.*?) (.*?) (ERROR) \\[.*?\\] \\[(.*?) - (.*?)\\] - ";
		String secondpattern = "([0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9])";

		// Matcher the pattern
		Pattern pi = Pattern.compile(pattern);
		Pattern pis = Pattern.compile(secondpattern);

		System.out
				.println("File of the read that will be read " + fileNamePath);
		BufferedReader reader = new BufferedReader(new FileReader(fileNamePath));
		String line;
		line = reader.readLine();
		int errorIndex = 0;
		int lastErrorIndex = 0;
		while (line != null) {

			Matcher m = pi.matcher(line);

			// Matcher for date;

			if (m.matches()) {

				if (!errorsFromLogFile.containsKey(errorIndex)) {
					errorsFromLogFile.put(errorIndex, new ArrayList<String>());
					lastErrorIndex = errorIndex;
					errorIndex++;

					stackTrace = new ArrayList();
				}

				for (int i = 0; i < 6; i++) { // Foreach errorFromLogFile add
					// groups
					errorsFromLogFile.get(lastErrorIndex).add(m.group(i));
				}

				ControlCheck = true;
				line = reader.readLine();

			}

			if (ControlCheck) { // If Find error;
				Matcher mas = pis.matcher(line);
				if (mas.lookingAt()) { // Check if exist datetime;

					ControlCheck = false;
					errorsFromLogFile.get(lastErrorIndex).add(
							String.join("\n", stackTrace));
				} else { // Insert StackTrace in arrayList;

					stackTrace.add(line);
					System.out.println(line);

				}
			}

			line = reader.readLine();

		}

		try {
			reader.close();

			// Foreach Entry Map position get values of String [ Integer
			// position, List String]
			for (Map.Entry<Integer, List<String>> errorEntry : errorsFromLogFile
					.entrySet()) {

				List<String> errorData = errorEntry.getValue();

				ErrorWebsphere errorss = new ErrorWebsphere();
				errorss.setTime_occurred(errorData.get(0) + errorData.get(1));
				errorss.setWhere(errorData.get(3) + errorData.get(4));
				errorss.setMessage(errorData.get(6));

				ErrorsWebsphereList.add(errorss);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void readJson() throws IOException {

		Gson gson = new Gson();
		File filetoRead = FileUtils.getFile(filePath + fileName);

		BufferedReader reader = new BufferedReader(new FileReader(filetoRead));
		String line;
		line = reader.readLine();
		while (line != null) {

			if (line.contains("\"level\":\"error\"")) {

				ErrorNodejs error = gson.fromJson(line, ErrorNodejs.class);
				ErrorsNodejslist.add(error);

			}
			line = reader.readLine();
		}

		try {
			reader.close();

			System.out.println("Whe found " + ErrorsNodejslist.size()
					+ "errors");

			for (int i = 0; i < ErrorsNodejslist.size(); i++) {
				System.out.println(ErrorsNodejslist.get(i).getLevel());
				System.out.println(ErrorsNodejslist.get(i).getMessage());
				System.out.println(ErrorsNodejslist.get(i).getTimestamp());
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createValues() throws IOException {

		String URL = "bolt://localhost:7687";
		File filetoRead = FileUtils.getFile(filePath + fileName);
		File fileNamePath = FileUtils.getFile(filePath + "test.dat");
		Driver driver = GraphDatabase.driver(URL,
				AuthTokens.basic("neo4j", "antonio10"));
		Session session = driver.session();
		Transaction tx = session.beginTransaction();
		StringBuilder sb = new StringBuilder();
		String line;
		String query = "";

		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(
					filetoRead));

			while ((line = bufferedReader.readLine()) != null) {
				sb.append(" " + line);
			}
			query = sb.toString();

			// Sessions are lightweight and disposable connection wrappers.

			if (!fileNamePath.exists()) {
				try {

					try {
						tx.run(query);
						tx.success(); // Mark this write as successful.
						System.out.println("Created the Neo4j OPERATIONS");
						// session.close();
						try {
							File file = new File(filePath + "test.dat");
							file.createNewFile();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (Exception e) {
						tx.failure();
					} finally {
						tx.close();
					}
				} catch (Exception e) {
					session.close();
				}

			} else {
				System.out.println("Neo4j OPERATIONS already done");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
