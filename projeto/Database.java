package projeto;

import java.util.List;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.Transaction;

import loggers.ErrorNodejs;
import loggers.ErrorWebsphere;

public class Database {

	void connect() {

	}

	public void saveNodejsToDB(List<ErrorNodejs> errorListNodejs) {

		String URL = "bolt://localhost:7687";
		// Sessions are lightweight and disposable connection wrappers.
		Driver driver = GraphDatabase.driver(URL,
				AuthTokens.basic("neo4j", "antonio10"));
		Session session = driver.session();

		try {
			// Wrapping Cypher in an explicit transaction provides atomicity
			// and makes handling errors much easier.

			Transaction tx = session.beginTransaction();

			try {

				for (int i = 0; i < errorListNodejs.size(); i++) {

					tx.run("MATCH (n:NODEJS {mac_addr: '1234'})"
							+ " CREATE (e:ERROR_LOG {level:'"
							+ errorListNodejs.get(i).getLevel()
							+ "', datetime:'"
							+ errorListNodejs.get(i).getTimestamp()
							+ "', message:'"
							+ errorListNodejs.get(i).getMessage() + "'})"
							+ " CREATE (n)-[d:HAS_ERROR]->(e)");

				}
				tx.success(); // Mark this write as successful.
				System.out.println("Nodes created for Nodejs Errors");

			} catch (Exception ea) {
				tx.failure();
			} finally {
				tx.close();
			}

		} catch (Exception e) {
			session.close();
			;
		}

	}

	public void saveWebsphereErrorstoDB(List<ErrorWebsphere> errorsWebsphereList) {

		String URL = "bolt://localhost:7687";
		// Sessions are lightweight and disposable connection wrappers.
		Driver driver = GraphDatabase.driver(URL,
				AuthTokens.basic("neo4j", "antonio10"));
		Session session = driver.session();
		Transaction tx = session.beginTransaction();

		try {
			// Wrapping Cypher in an explicit transaction provides atomicity
			// and makes handling errors much easier.

			try {
				for (int i = 0; i < errorsWebsphereList.size(); i++) {

					tx.run("MATCH (n:WEBSPHERE {mac_addr: '1234444'})"
							+ " CREATE (e:ERROR_LOG {datetime:'"
							+ errorsWebsphereList.get(i).getTime_occurred()
							+ "', where:'"
							+ errorsWebsphereList.get(i).getWhere()
							+ "', message:'"
							+ errorsWebsphereList.get(i).getMessage() + "'})"
							+ " CREATE (n)-[d:HAS_ERROR]->(e)");

				}
				tx.success(); // Mark this write as successful.
				System.out.println("Nodes created for Websphere");
			} catch (Exception ea) {
				tx.failure();
			} finally {
				tx.close();
			}

		} catch (Exception e) {
			session.close();

		}

	}
}
