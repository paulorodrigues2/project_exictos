package loggers;

public class ErrorWebsphere {

	// private int id = 0;
	private String time_occurred;
	private String where;
	private String message;

	public ErrorWebsphere(String time_occurred, String where, String message) {
		super();

		this.time_occurred = time_occurred;
		this.where = where;
		this.message = message;
	}

	/*
	 * private int getId() { return id; }
	 * 
	 * 
	 * private void setId(int id) { this.id = id; }
	 */

	public ErrorWebsphere() {
		// TODO Auto-generated constructor stub
	}

	public String getTime_occurred() {
		return time_occurred;
	}

	public void setTime_occurred(String time_occurred) {
		this.time_occurred = time_occurred;
	}

	public String getWhere() {
		return where;
	}

	public void setWhere(String where) {
		this.where = where;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
