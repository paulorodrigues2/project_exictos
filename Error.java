package projeto;

public class Error {

	private String id;
	private String time_occurred;
	private String where;
	private String message;
	
	public Error(String id, String time_occurred, String where, String message) {
		super();
		this.id = id;
		this.time_occurred = time_occurred;
		this.where = where;
		this.message = message;
	}
	
	private String getId() {
		return id;
	}
	
	
	void setId(String id) {
		this.id = id;
	}
	
	private String getTime_occurred() {
		return time_occurred;
	}
	private void setTime_occurred(String time_occurred) {
		this.time_occurred = time_occurred;
	}
	private String getWhere() {
		return where;
	}
	private void setWhere(String where) {
		this.where = where;
	}
	private String getMessage() {
		return message;
	}
	private void setMessage(String message) {
		this.message = message;
	}
	
}
	
	
	